
- Fill in .env-example, rename to .env. 
- Update correct repo and image names in `csccp.js` script, and in the `deployTemplate.yaml`
- To start `npm start`
- To view logs, you can run `npm run list` and then view the corresponding log file
- To stop, `npm stop`
