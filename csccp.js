const exec = require('await-exec')
const express = require('express')
const app = express()
const port = 2344

require('dotenv').config();
if(!process.env.authToken){
	console.error("No Auth token set!! – fill out .env-example and rename to .env!");
}

app.get('/cscclassprofilestaging', async (req, res) => {
	console.log("Received build request");

	const buildId = req.query.ref;
	const name = `${buildId}-csc-class-profile-staging`
	const publicUrl = `${name}-snedadah.k8s.csclub.cloud`
	const image = `registry.cloud.csclub.uwaterloo.ca/snedadah/csc-class-profile-staging:${buildId}`


	if (!req.headers.authorization) {
		console.log("No auth!");
		return res.status(403).json({ error: 'No credentials sent!' });
  	}

	if(req.headers.authorization != process.env.authToken){
		console.log("Invalid auth!");
		return res.status(401).json({error: 'Invalid Auth Token!'});
	}



	// command for generating deployment template
	//await call(`kubectl create deployment ${name} --image ${image} --port=80 -o yaml --dry-run > template.yaml`);

	console.log(`${(new Date()).toUTCString()} - Doing deploy for for branch: ${req.query.ref}`);

	// try deleting incase it already exists
	await call(`kubectl delete deployment ${name}`);

	// replace variables in template with correct name and image
	await call(`cat deployTemplate.yaml | sed "s@__NAME__@${name}@g" | sed "s@__IMAGE__@${image}@g" | kubectl apply -f -`);

	await call(`kubectl expose deployment ${name}`);
	await call(`kubectl create ingress ${name} --rule='${publicUrl}/*=${name}:80'`);

	console.log("Deployed to " + publicUrl);
	res.send(publicUrl);
})

async function call(cmd){
	let out
	try{
		out = await exec(cmd);
		console.log(out)
	}catch(e)
	{
		console.log(e, out)
	}
}

app.listen(port, () => {
	console.log(`Listening on port ${port}`)
})
